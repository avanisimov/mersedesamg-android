package com.suxelshock.mersedesamg.view;

import android.content.Context;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;

public class Speedometer extends View {

	private static final int indigo = Color.parseColor("#3f51b5");
	private static final int tealA200Color = Color.parseColor("#64ffda");
	private static final int greenA200Color = Color.parseColor("#5af158");
	private static final int green500 = Color.parseColor("#259b24");
	private static final int yellow = Color.parseColor("#ffff00");
	private static final int orange = Color.parseColor("#ff9800");
	private static final int deepOrange = Color.parseColor("#ff5722");
	private static final int red500 = Color.parseColor("#e51c23");
	private static final int[] colors = new int[] { indigo, tealA200Color,
			greenA200Color, yellow, orange, orange,deepOrange, red500,red500,red500 };
	
	private static final int MIN_SIZE_FOR_EXTERNAL_TEXT = 300;
	private final int globalStartAngle = 30;
	
	// global user settings	
	private boolean arrowAnimation = false;
	private boolean textAnimation = false;	
	
	private float maxScaleValue = 100;
	private float currentScaleValue = 60;	
	
	private int scaleWidth = 10;
	private int scaleDivWidth = 10;

	public Speedometer(Context context) {
		super(context);
	}

	public Speedometer(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Speedometer(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public void setMaxScaleValue(int maxScaleValue) {
		this.maxScaleValue = maxScaleValue;
	}
	
	public void setArrowAnimation(boolean arrowAnimation) {
		this.arrowAnimation = arrowAnimation;
	}
	
	public void setTextAnimation(boolean textAnimation) {
		this.textAnimation = textAnimation;
	}
	
	public void setCurrentScaleValue(int value) {
		if (arrowAnimation) {
			SpeedAnimation sa = new SpeedAnimation(currentScaleValue, value);
			startAnimation(sa);	
		} else {
			currentScaleValue = value;
			invalidate();
		}		
	}

	private int size = 0;
	private int paddingLeft = 0;
	private int paddingRight = 0;
	private int paddingTop = 0;
	private int paddingBottom = 0;
	private float centerX = 0;
	private float centerY = 0;
	private int angleDelta;
	private int startAngle;
	private int segmentsCount = 10;
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int defW = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
		int defH = getDefaultSize(getSuggestedMinimumHeight(),
				heightMeasureSpec);
		size = Math.min(defW, defH);
		setMeasuredDimension(size, size);
		measureDimens();
		
	}	

	private void measureDimens() {
		int textDelta = 20;
		if (size < MIN_SIZE_FOR_EXTERNAL_TEXT) {
			textDelta = 20;
		} else {
			textDelta = 0;
		}
		paddingLeft = getPaddingLeft() + textDelta;
		paddingRight = getPaddingRight() + textDelta;
		paddingTop = getPaddingTop() + textDelta;
		paddingBottom = getPaddingBottom() + textDelta;
		
		centerX = size / 2;
		centerY = size / 2;
		angleDelta = (360 - globalStartAngle * 2) / (segmentsCount);
		startAngle = 90 + globalStartAngle;
		arrowLength = (size - paddingRight - paddingLeft) / 2;
		
		textSize = size / 15;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		drawDial(canvas);
		drawArrow(canvas);
		drawText(canvas);
	}
	
	
	// ============================================
	// Dial
	// ============================================
	private RectF scaleRect = new RectF();
	private RectF scaleDivRect = new RectF();
	private Paint scalePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	{
		scalePaint.setStrokeWidth(scaleWidth);
		scalePaint.setStyle(Paint.Style.STROKE);
	}
	
	private final int shadowDelta = 5;
	private RectF scaleShadowRect = new RectF();
	private Paint scaleShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	{
		scaleShadowPaint.setStrokeWidth(4);
		scaleShadowPaint.setStyle(Paint.Style.STROKE);
		scaleShadowPaint.setColor(Color.GRAY);
		scaleShadowPaint.setMaskFilter(new BlurMaskFilter(5,
				BlurMaskFilter.Blur.NORMAL));
	}
	
	private void drawDial(Canvas canvas) {
		scaleRect.set(
				paddingLeft,
				paddingTop,
				size - paddingRight,
				size - paddingBottom);
		scaleDivRect.set(
				paddingLeft + scaleDivWidth - 1,
				paddingTop + scaleDivWidth - 1,
				size - paddingRight - scaleDivWidth + 1,
				size - paddingBottom - scaleDivWidth + 1);
		scaleShadowRect.set(
				paddingLeft + shadowDelta,
				paddingTop + shadowDelta,
				size - paddingRight + shadowDelta,
				size - paddingBottom + shadowDelta);

		int dialStartAngle = startAngle;
		for (int i = 0;i < segmentsCount; i++) {
			canvas.drawArc(scaleShadowRect, dialStartAngle, angleDelta, false, scaleShadowPaint);

			scalePaint.setColor(colors[i]);
			canvas.drawArc(scaleRect, dialStartAngle - 0.5f, angleDelta + 0.5f, false, scalePaint);
			canvas.drawArc(scaleDivRect, dialStartAngle - 0.5f, 2, false, scalePaint);
			
			dialStartAngle += angleDelta;			
			canvas.drawArc(scaleDivRect, dialStartAngle - 2, 2, false, scalePaint);

		}
	}
	// ============================================
	// Labels
	// ============================================
	private void drawLabels(Canvas canvas) { 
		
	}
	
	// ============================================
	// Arrow
	// ============================================
	private double arrowAngle;
	private int arrowLength;
	private float centerRadius = 50;
	private Paint arrowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	{
		arrowPaint.setAntiAlias(true);
		arrowPaint.setColor(green500);
		arrowPaint.setStrokeWidth(5);
	}
	private Paint arrowCircleShadow = new Paint();
	{
		arrowCircleShadow.setAntiAlias(true);
		arrowCircleShadow.setColor(Color.GRAY);
		arrowCircleShadow.setMaskFilter(new BlurMaskFilter(5,
				BlurMaskFilter.Blur.NORMAL));
	}
	
	private static double plus90 = Math.toRadians(90);
	private static double minus90 = Math.toRadians(-90);

	private void drawArrow(Canvas canvas) {
		arrowAngle = Math.toRadians(90 + globalStartAngle + (360-globalStartAngle*2)*((float)(currentScaleValue) / maxScaleValue));
		centerRadius = 0.1f * size;
		
		float arrowX1 = (float) (centerX + (arrowLength) * Math.cos(arrowAngle));
		float arrowY1 = (float) (centerY + (arrowLength) * Math.sin(arrowAngle));
		
		float arrowX2 = (float) (centerX + (centerRadius*0.5) * Math.cos(arrowAngle+plus90));
		float arrowY2 = (float) (centerY + (centerRadius*0.5) * Math.sin(arrowAngle+plus90));
		
		float arrowX3 = (float) (centerX + (centerRadius*0.5) * Math.cos(arrowAngle+minus90));
		float arrowY3 = (float) (centerY + (centerRadius*0.5) * Math.sin(arrowAngle+minus90));
		
		//canvas.drawLine(arrowX, arrowY, centerX, centerY, arrowPaint);
		Path path = new Path();
		path.moveTo(arrowX1, arrowY1);
		path.lineTo(arrowX2, arrowY2);
		path.lineTo(arrowX3, arrowY3);
		path.lineTo(arrowX1, arrowY1);
		
		canvas.drawPath(path, arrowPaint);		
		
		canvas.drawCircle(centerX + 3, centerY + 3, centerRadius,
				arrowCircleShadow);
		canvas.drawCircle(centerX, centerY, centerRadius, arrowPaint);
	}
	
	// ============================================
	// Digits
	// ============================================
	
	private Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	{
		textPaint.setTextAlign(Align.CENTER);
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"Retropecan-Light.ttf");
		textPaint.setTypeface(Typeface.SANS_SERIF);		
	}
	private float textSize;
	private void drawText(Canvas canvas) {
		int textStartAngle = startAngle;
		textPaint.setColor(Color.BLACK);		
		textPaint.setTextSize(textSize);
		for (int i = 0; i <= segmentsCount; i++) {
			if (i >0) {
				textPaint.setColor(colors[i-1]);
			}
			
			if (textAnimation) {
				float value = i * (maxScaleValue / segmentsCount);
				float delta = Math.abs(value - currentScaleValue);
				if (delta <= 5) {				
					textPaint.setTextSize(textSize + (5 - delta) * 5);
				} else {
					textPaint.setTextSize(textSize);
				}
			}			
			
			double textAngle = Math.toRadians(textStartAngle);
			if (size < MIN_SIZE_FOR_EXTERNAL_TEXT) {
				float textX = (float) (centerX + (centerX * 0.9) * Math.cos(textAngle));
				float textY = (float) (centerY*1.05 + (centerX * 0.9) * Math.sin(textAngle));
				canvas.drawText(String.valueOf(i * 10), textX, textY, textPaint);
			} else {
				float textX = (float) (centerX + (centerX * 0.75) * Math.cos(textAngle));
				float textY = (float) (centerY*1.05 + (centerX * 0.75) * Math.sin(textAngle));
				canvas.drawText(String.valueOf(i * 10), textX, textY, textPaint);
			}
			
			textStartAngle += angleDelta;
			
			
		}
		
	}
	
	private class SpeedAnimation extends Animation {
		final int DURATION = 1000;
		float from;
		float to;
		
		public SpeedAnimation(float fromValue, float toValue) {
			this.from = fromValue;
			this.to = toValue;
			setDuration(DURATION);
			setInterpolator(new Interpolator() {
				
				@Override
				public float getInterpolation(float input) {
					
					return (float) Math.pow(input, (1/3.0));
				}
			});
		}
		
		@Override
		protected void applyTransformation(float interpolatedTime,
				Transformation t) {
			currentScaleValue = (from + (to-from) * interpolatedTime);
			invalidate();
		
		}
		
	}
	
}
