package com.suxelshock.mersedesamg.view;

import com.suxelshock.mersedesamg.view.WheelView.WheelAngleListener;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class WheelAnimation extends Animation {

	private WheelAngleListener listener;
	public WheelAnimation(float fromDeegress, WheelAngleListener wheelAngleListener) {
		super();
		this.listener = wheelAngleListener;
		degrees = fromDeegress;
		setInterpolator(new AccelerateDecelerateInterpolator());
		setDuration(300);
	}

	private float px;
	private float py;

	@Override
	public void initialize(int width, int height, int parentWidth,
			int parentHeight) {
		super.initialize(width, height, parentWidth, parentHeight);
		px = (float) width / 2;
		py = (float) height / 2;
	}

	float degrees = 0;

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		super.applyTransformation(interpolatedTime, t);
		float newAngle = degrees * (1 - interpolatedTime);
		t.getMatrix().setRotate(newAngle, px, py);
		if (listener != null) {
			listener.onWheelAngleChanged((int)newAngle);
		}
	}

}
