package com.suxelshock.mersedesamg.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.suxelshock.mersedesamg.R;

public class WheelView extends View {

	private int resourceId = R.drawable.wheel_blue;
	private WheelAngleListener wheelAngleListener;

	public WheelView(Context context) {
		super(context);
		bitmap = BitmapFactory.decodeResource(getResources(), resourceId);
	}

	public WheelView(Context context, AttributeSet attrs) {
		super(context, attrs);
		bitmap = BitmapFactory.decodeResource(getResources(), resourceId);
	}

	public WheelView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		bitmap = BitmapFactory.decodeResource(getResources(), resourceId);
	}

	private int size;

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int defW = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
		int defH = getDefaultSize(getSuggestedMinimumHeight(),
				heightMeasureSpec);
		size = Math.min(defW, defH);
		setMeasuredDimension(size, size);
		bitmap = Bitmap.createScaledBitmap(bitmap, size, size, true);
	}

	public void setResourceId(int resId) {
		this.resourceId = resId;
		bitmap = BitmapFactory.decodeResource(getResources(), resourceId);
		// bitmap = Bitmap.createScaledBitmap(bitmap, size, size, true);
		invalidate();
	}

	private Bitmap bitmap;
	private Matrix matrix = new Matrix();
	private float angle = 0;
	private Paint bitmapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	@Override
	protected void onDraw(Canvas canvas) {
		if (bitmap != null) {

			matrix.setRotate(angle, (float) size / 2, (float) size / 2);
			canvas.drawBitmap(bitmap, matrix, new Paint(Paint.ANTI_ALIAS_FLAG));
		}
	}

	public static Bitmap rotateBitmap(Bitmap source, float angle) {
		Matrix matrix = new Matrix();
		
		
		matrix.postRotate(-angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
				source.getHeight(), matrix, true);
	}

	float startAngle = 0;

	public void setWheelAngleListener(WheelAngleListener wheelAngleListener) {
		this.wheelAngleListener = wheelAngleListener;
	}
	
	public void removeWheelAngleListener() {
		this.wheelAngleListener = null;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		float x = event.getX();
		float y = event.getY();
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			startAngle = getAngle(x, y);
			
			return true;
		case MotionEvent.ACTION_UP:
			startAnimation(new WheelAnimation(angle, wheelAngleListener));
			angle = 0;
			return true;

		case MotionEvent.ACTION_MOVE:
			float currAngle = getAngle(x, y);
			angle += startAngle - currAngle;
			startAngle = currAngle;			
			invalidate();
			if (wheelAngleListener != null) {
				wheelAngleListener.onWheelAngleChanged((int) angle);
			}
			return true;

		default:
			return super.onTouchEvent(event);
		}

	}

	private float getAngle(float x, float y) {
		x = x - size / 2;
		y = y - size / 2;
		float result = (float) Math.toDegrees(Math.atan2(y, x));
		if (result < 0) {
			result =  Math.abs(result);
		} else {
			result =  360 - result;	
		}
		result = (result + 270) % 360;
		return result;

	}
	
	
	
	public static interface WheelAngleListener {
	
		void onWheelAngleChanged(int angle) ;
	}
	
}
