package com.suxelshock.mersedesamg.settings;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref
public interface SettingsStore {

	@DefaultInt(9600)
	int baudrate();

	@DefaultInt(8)
	int bitsCount();

	@DefaultInt(1)
	int stopBitsCount();

}
