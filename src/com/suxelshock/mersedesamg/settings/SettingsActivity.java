package com.suxelshock.mersedesamg.settings;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NoTitle;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

import com.suxelshock.mersedesamg.R;

@EActivity(R.layout.activity_settings)
public class SettingsActivity extends Activity {

	@Pref
	SettingsStore_ settings;
	@StringArrayRes(R.array.baudrate_values)
	protected String[] baudrates;
	@ViewById(R.id.activity_settings_txtBaudrate)
	protected TextView txtBaudrate;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@AfterViews
	protected void afterViews() {
		txtBaudrate.setText(String.valueOf(settings.baudrate().get()));
	}

	@Click(R.id.activity_settings_vgpBaudrate)
	protected void onClickBaudrate() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.textSettingsUartBaudrateTitle)
				.setSingleChoiceItems(baudrates, -1,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								settings.edit()
										.baudrate()
										.put(Integer.parseInt(baudrates[which]));
							}
						});
	}


}
