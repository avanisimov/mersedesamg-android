package com.suxelshock.mersedesamg;

public interface MainModelListener {

	public void onWheelValueChanged(byte wheelValue);

	public void onSpeedValueChanged(byte speedValue);

}
