package com.suxelshock.mersedesamg;

import java.util.Timer;
import java.util.TimerTask;

import org.androidannotations.annotations.EBean;

import android.content.Context;
import android.util.Log;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.FT_Device;

@EBean
public class MainModel {

	private final static String LOG_TAG = "MainModel";
	private final static int SERVO_TIMER_PERIOD = 50;

	private static D2xxManager ftD2xx = null;
	private FT_Device ftDev;

	private MainModelListener listener;

	private byte wheelValue;
	private boolean wheelAnimation = true;
	private byte speedValue;
	private boolean speedAnimation = true;
	private Timer servoTimer = new Timer();

	public MainModel(Context context) {
		try {
			ftD2xx = D2xxManager.getInstance(context);
		} catch (D2xxManager.D2xxException ex) {
			Log.e(LOG_TAG, ex.toString());
		}
		servoTimer.schedule(servoTimerTask, 100, SERVO_TIMER_PERIOD);
	}

	public void setListener(MainModelListener listener) {
		this.listener = listener;
	}

	public void removeListener() {
		this.listener = null;
	}

	public byte getWheelValue() {
		return wheelValue;
	}

	public void setWheelValue(byte wheelValue) {
		this.wheelValue = wheelValue;
	}

	public byte getSpeedValue() {
		return speedValue;
	}

	public void setSpeedValue(byte speedValue) {
		this.speedValue = speedValue;
	}

	public void disableWheelAnimation() {
		this.wheelAnimation = false;
	}

	public void enableWheelAnimation() {
		this.wheelAnimation = true;

	}

	private void notifyWheelValueChanged() {
		if (listener != null) {
			listener.onWheelValueChanged(wheelValue);
		}
	}

	private void notifySpeedValueChanged() {
		if (listener != null) {
			listener.onSpeedValueChanged(speedValue);
		}
	}

	private TimerTask servoTimerTask = new TimerTask() {

		@Override
		public void run() {
			if (wheelAnimation) {
				if (wheelValue > 0) {
					wheelValue -= wheelValue / 10 + 1;
					notifyWheelValueChanged();
				} else if (wheelValue > 0) {
					wheelValue += wheelValue / 10 - 1;
					notifyWheelValueChanged();
				}
			}

			if (speedAnimation) {
				if (speedValue > 0) {
					speedValue -= speedValue / 10 + 1;
					notifySpeedValueChanged();
				} else if (wheelValue > 0) {
					speedValue += speedValue / 10 - 1;
					notifySpeedValueChanged();
				}
			}
		}

	};

}
