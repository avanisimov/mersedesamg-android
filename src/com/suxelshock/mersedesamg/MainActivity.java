package com.suxelshock.mersedesamg;

import java.util.Timer;
import java.util.TimerTask;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.suxelshock.mersedesamg.service.Mersedes;
import com.suxelshock.mersedesamg.service.MersedesListener;
import com.suxelshock.mersedesamg.service.Mersedes_;
import com.suxelshock.mersedesamg.view.Speedometer;
import com.suxelshock.mersedesamg.view.WheelAnimation;
import com.suxelshock.mersedesamg.view.WheelView;
import com.suxelshock.mersedesamg.view.WheelView.WheelAngleListener;

@WindowFeature(Window.FEATURE_NO_TITLE)
@EActivity(R.layout.activity_main)
public class MainActivity extends Activity implements MersedesListener, WheelAngleListener {
	private final static String TAG = "MainActivity";

	private Intent service;
	private boolean bound = false;
	private ServiceConnection sConn;
	private Mersedes mersedes;
	
	@ViewById(R.id.activity_main_speedometer)
	protected Speedometer speedometer;
	
	@ViewById(R.id.activity_main_wheelview)
	protected WheelView wheelView;

	@ViewById(R.id.activity_main_skbrWheel)
	protected SeekBar skbrWheel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		service = new Intent(this, Mersedes_.class);
		sConn = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d(TAG, "MainActivity onServiceConnected");
				Mersedes.Binder mBinder = (Mersedes.Binder) binder;
				mersedes = mBinder.getService();
				mersedes.setListener(MainActivity.this);
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d(TAG, "MainActivity onServiceDisconnected");
				bound = false;
			}
		};
		startService(service);
		bindService(service, sConn, BIND_AUTO_CREATE);

	}

	@AfterViews
	protected void afterViews() {
		skbrWheel.setMax(254);
		// skbrWheel.setProgress(model.getWheelValue() + Byte.MAX_VALUE);
		skbrWheel.setOnSeekBarChangeListener(wheelListener);
		// model.setListener(this);
		timer.schedule(task, 10, 500);
		wheelView.setWheelAngleListener(this);
		speedometer.setArrowAnimation(true);
		speedometer.setTextAnimation(true);
	}
	Timer timer = new Timer();
	TimerTask task = new TimerTask() {
		float degree = 110;
		int value = 100;
		int modificator = 1;
		@Override
		public void run() {
//			//setSpeedometerAngle(degree);
//			degree +=25;
//			if (degree > 430) {
//				degree = 110;
//			}
//			setSpeedometerAngle(degree);
			//value += modificator;
//			if (value == 100) {
//				value = 0;
//			}else if (value == 0) {
//				value = 100;
//			}
			value = (int) (Math.random()*100);
			setSpeedometerAngle(value);
		}
	};
	
	@UiThread
	protected void setSpeedometerAngle(float degree) {
		//speedometer.setArrowAngle(degree);
		speedometer.invalidate();

	}
	
	@UiThread
	protected void setSpeedometerAngle(int value) {
		speedometer.setCurrentScaleValue(value);

	}
	
	@Click(R.id.activity_main_btnSettings)
	protected void onClickSettings() {
		Intent settings = new Intent(this,
				com.suxelshock.mersedesamg.settings.SettingsActivity_.class);
		startActivity(settings);
	}

	@UiThread
	@Override
	public void onWheelValueChanged(byte wheelValue) {
		skbrWheel.setProgress(wheelValue + Byte.MAX_VALUE);
	}

	@UiThread
	@Override
	public void onSpeedValueChanged(byte speedValue) {

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mersedes.removeListener(this);
		unbindService(sConn);
		stopService(service);
		timer.cancel();
		// unregisterReceiver(mUsbReceiver);
	}

	private OnSeekBarChangeListener wheelListener = new OnSeekBarChangeListener() {

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			//mersedes.enableWheelAnimation();
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			//mersedes.freezeWheelAnimation();
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			byte value = (byte) (progress - Byte.MAX_VALUE);
			//mersedes.setWheelValue(value);
		}
	};

	@Override
	public void onWheelAngleChanged(int angle) {
		if (angle > 100) {
			angle = 100;
		}
		if (angle > -100) {
			angle = -100;
		}
		mersedes.setWheelValue(angle);
		
	}

	// BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
	// public void onReceive(Context context, Intent intent) {
	//
	// String action = intent.getAction();
	// if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
	// // never come here(when attached, go to onNewIntent)
	// // openDevice();
	// } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
	// // closeDevice();
	// }
	// }
	// };

}
