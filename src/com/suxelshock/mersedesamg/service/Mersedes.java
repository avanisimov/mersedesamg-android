package com.suxelshock.mersedesamg.service;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.androidannotations.annotations.EService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.D2xxManager.D2xxException;
import com.ftdi.j2xx.FT_Device;
import com.suxelshock.mersedesamg.settings.SettingsStore_;

@EService
public class Mersedes extends Service {

	private static final String TAG = "Mersedes_service";

	private static final int THREAD_COUNT = 5;

	private static final int code = 0;
	private static final int value = 1;
	private static final byte[] COMMAND_WHEEL = { 0x35, 0 };
	private static final byte[] COMMAND_SENT_SPEED = { 0x36, 0 };
	private static final byte[] COMMAND_REAL_SPEED = { 0x37, 0 };
	private static final byte[] COMMAND_GET_RPM = { 0x38, 0 };
	private static final byte[] COMMAND_GET_TEMP = { 0x39, 0 };

	private static final long WHEEL_SPEED_PERIOD = 20;

	private MersedesListener mListener;
	@Pref
	protected SettingsStore_ settings;

	private D2xxManager manager;
	private FT_Device dev;

	private ScheduledExecutorService schedule = Executors
			.newSingleThreadScheduledExecutor();

	private byte sentWheel;
	private Runnable sentWheelThread = new Runnable() {

		@Override
		public void run() {
			if (dev != null && dev.isOpen()) {
				COMMAND_WHEEL[value] = sentWheel;
				dev.write(COMMAND_WHEEL);
			}

		}
	};
	private byte sentSpeed;
	private Runnable sentSpeedThread = new Runnable() {

		@Override
		public void run() {
			if (dev != null && dev.isOpen()) {
				COMMAND_SENT_SPEED[value] = sentSpeed;
				dev.write(COMMAND_SENT_SPEED);
			}
		}
	};
	private int realSpeed;
	private Runnable realSpeedThread = new Runnable() {

		@Override
		public void run() {
			if (dev != null && dev.isOpen()) {
				dev.write(COMMAND_REAL_SPEED);
				// read

			}
		}
	};
	private int realRPM;
	private Runnable realRPMThread = new Runnable() {

		@Override
		public void run() {
			if (dev != null && dev.isOpen()) {
				dev.write(COMMAND_REAL_SPEED);
				// read

			}
		}
	};
	private int realTemperature;
	private Runnable realTempThread = new Runnable() {

		@Override
		public void run() {
			if (dev != null && dev.isOpen()) {
				dev.write(COMMAND_REAL_SPEED);
				// read

			}
		}
	};

	@Override
	public void onCreate() {
		super.onCreate();
		try {
			manager = D2xxManager.getInstance(this);
		} catch (D2xxException e) {
			Log.e(TAG, "" + e.getMessage());
		}

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		tryToOpenDevice();
		if (dev != null && dev.isOpen()) {
			schedule.scheduleAtFixedRate(sentWheelThread, 6, 10,
					TimeUnit.MILLISECONDS);
			schedule.scheduleAtFixedRate(sentSpeedThread, 3, 10,
					TimeUnit.MILLISECONDS);
		}
		return super.onStartCommand(intent, flags, startId);
	}

	private void tryToOpenDevice() {
		try {
			int count = manager.createDeviceInfoList(this);
			if (count > 0) {
				dev = manager.openByIndex(this, 0);
				if (dev != null && dev.isOpen()) {
					int baudRate = settings.baudrate().get();
					byte bits = (byte) settings.bitsCount().get();
					byte stopbits = (byte) settings.stopBitsCount().get();
					byte parity = (byte) 0;
					byte flowControl = D2xxManager.FT_FLOW_NONE;

					dev.setBaudRate(baudRate);
					dev.setDataCharacteristics(bits, stopbits, parity);
				}
			}
		} catch (Exception e) {

		}

	}

	public void setSentWheelValue(int progress) {
		sentWheel = (byte) (progress);
	}

	@UiThread
	void notifyWheelValueChanged() {
		if (mListener != null) {
			mListener.onWheelValueChanged(sentWheel);
		}
	}

	@UiThread
	void notifySentSpeedChanged() {

	}

	@UiThread
	void notifyRealSpeedChanged() {

	}

	@UiThread
	void notifyRPMChanged() {

	}

	@UiThread
	void notifyTemperatureChanged() {

	}

	public void setListener(MersedesListener listener) {
		mListener = listener;
		notifyWheelValueChanged();
	}

	public void removeListener(MersedesListener listener) {
		mListener = null;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return new Binder(this);
	}

	public static class Binder extends android.os.Binder {

		private Mersedes mersedes;

		public Binder(Mersedes mersedes) {
			this.mersedes = mersedes;
		}

		public Mersedes getService() {
			return mersedes;
		}

	}

	private void closeDevice() {
		if (dev != null) {
			dev.close();
		}
	}

	public void onDestroy() {
		closeDevice();
	}
}
