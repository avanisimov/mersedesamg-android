package com.suxelshock.mersedesamg.service;

public interface MersedesListener {

	void onWheelValueChanged(byte wheelValue);

	void onSpeedValueChanged(byte speedValue);
}
