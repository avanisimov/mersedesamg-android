//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package com.suxelshock.mersedesamg.settings;

import android.content.Context;
import android.content.SharedPreferences;
import org.androidannotations.api.sharedpreferences.EditorHelper;
import org.androidannotations.api.sharedpreferences.IntPrefEditorField;
import org.androidannotations.api.sharedpreferences.IntPrefField;
import org.androidannotations.api.sharedpreferences.SharedPreferencesHelper;

public final class SettingsStore_
    extends SharedPreferencesHelper
{

    private Context context_;

    public SettingsStore_(Context context) {
        super(context.getSharedPreferences((getLocalClassName(context)+"_SettingsStore"), 0));
        this.context_ = context;
    }

    public SettingsStore_.SettingsStoreEditor_ edit() {
        return new SettingsStore_.SettingsStoreEditor_(getSharedPreferences());
    }

    private static String getLocalClassName(Context context) {
        String packageName = context.getPackageName();
        String className = context.getClass().getName();
        int packageLen = packageName.length();
        if (((!className.startsWith(packageName))||(className.length()<= packageLen))||(className.charAt(packageLen)!= '.')) {
            return className;
        }
        return className.substring((packageLen + 1));
    }

    public IntPrefField baudrate() {
        return intField("baudrate", 9600);
    }

    public IntPrefField bitsCount() {
        return intField("bitsCount", 8);
    }

    public IntPrefField stopBitsCount() {
        return intField("stopBitsCount", 1);
    }

    public final static class SettingsStoreEditor_
        extends EditorHelper<SettingsStore_.SettingsStoreEditor_>
    {


        SettingsStoreEditor_(SharedPreferences sharedPreferences) {
            super(sharedPreferences);
        }

        public IntPrefEditorField<SettingsStore_.SettingsStoreEditor_> baudrate() {
            return intField("baudrate");
        }

        public IntPrefEditorField<SettingsStore_.SettingsStoreEditor_> bitsCount() {
            return intField("bitsCount");
        }

        public IntPrefEditorField<SettingsStore_.SettingsStoreEditor_> stopBitsCount() {
            return intField("stopBitsCount");
        }

    }

}
